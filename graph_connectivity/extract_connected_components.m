function [  ] = extract_connected_components( NAME )
%NAME should be a string with the name of the nifti you're interested in.


X=load_untouch_nii(NAME);
IMG=X.img;
%change height threshold
thresh=.5;
[Conn, Weight] = Create_Graph_Connectivity(thresh,IMG);

Conn_thresh_weight = ((Conn  > thresh).*Weight);
G=graph(Conn_thresh_weight);
bins=conncomp(G);
binsu=unique(bins);

[N,edges]=histcounts(bins,[binsu, max(binsu(:))+1]);

big_clusters= find(N>1);

Voxels = ismember(bins,big_clusters);
Voxels_labeled=Voxels.*bins;

[not,not2,Voxels_ranked] = unique(Voxels_labeled);




X.img(:)=Voxels_ranked-1;
save_untouch_nii(X,'components.nii');

%[N,EDGES] = histcounts(bins,binsu);
%%change voxel extent threshold below
%L = (N > 150);
%T=find(L);
%NEWIMG=0*IMG;

%for i=1:length(T)
%S( bins==EDGES(T(i))) =1;
%end
%for i=1:length(S)
%NEWIMG(i)=S(i)*IMG(i);
%end
%X.img=NEWIMG;
%save_untouch_nii(X, 'tau_threshold.nii');
