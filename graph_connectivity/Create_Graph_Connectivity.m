function [ Conn, Weight ] = Create_Graph_Connectivity( ABSTH, IMG )

%A function to create a connectivity (and weighing) graph from MRI data. This gives an accurate depiction of cortical distance and allows for volumetric smoothing without touching neighboring gyrii, among other things.


% Padding the matrix to avoid indexing errors and determining parameters.

L=size(IMG);
MASKE=0.*IMG;
T=prod(L);

%Eroding outside to avoid errors...

MASKE(2:L(1)-1,2:L(2)-1,2:L(3)-1)=1;
IMG=MASKE.*IMG;
IMG=double(IMG);
Expanded=zeros(L(1)+2,L(2)+2,L(3)+2);
Expanded(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) = IMG;



display('Creating Adjacency Matrix...')



% Wiping out signal below a defined threshold. 
A=Expanded > ABSTH;

display('Single Shifts...')


% Adjacency is determined through a simple process. Two voxels are connected if BOTH of them have intensity 
% above a certain threshold (to be dynamically defined later, the thresholds themselves are what's listed, provided
% everything is above ABSTHR

% To determine this, we shift the matrix and then add it to itself. This is done 26 times
% for the 26 neighbors of each Vertex. The code is a bit repetetive, but since it's
% just addition, it's fast. P.S. if you're smarter than I am and want to do this with 
% Regular expressions, please do!

ONE = circshift(A, -1, 1)+A;
TWO = circshift(A, -1, 2) +A;
THREE = circshift(A, -1, 3) +A;
MONE = circshift(A, 1, 1)+A;
MTWO = circshift(A, 1, 2) +A;
MTHREE = circshift(A, 1, 3) +A;

display('Double Shifts...')

ONE_TWO=circshift(circshift(A,-1,1),-1,2) + A;
ONE_MTWO=circshift(circshift(A,-1,1),1,2) + A;
ONE_THREE=circshift(circshift(A,-1,1),-1,3) + A;
ONE_MTHREE=circshift(circshift(A,-1,1),1,3) + A;

MONE_TWO=circshift(circshift(A,1,1),-1,2) + A;
MONE_MTWO=circshift(circshift(A,1,1),1,2) + A;
MONE_THREE=circshift(circshift(A,1,1),-1,3) + A;
MONE_MTHREE=circshift(circshift(A,1,1),1,3) + A;

TWO_THREE=circshift(circshift(A,-1,2),-1,3) + A;
TWO_MTHREE=circshift(circshift(A,-1,2),1,3) + A;

MTWO_THREE=circshift(circshift(A,1,2),-1,3) + A;
MTWO_MTHREE=circshift(circshift(A,1,2),1,3) + A;

display('Triple Shifts...')

ONE_TWO_THREE=circshift(circshift(circshift(A,-1,1),-1,2),-1,3) + A;
ONE_TWO_MTHREE=circshift(circshift(circshift(A,-1,1),-1,2),1,3) + A;
ONE_MTWO_THREE=circshift(circshift(circshift(A,-1,1),1,2),-1,3) + A;
ONE_MTWO_MTHREE=circshift(circshift(circshift(A,-1,1),1,2),1,3) + A;

MONE_TWO_THREE=circshift(circshift(circshift(A,1,1),-1,2),-1,3) + A;
MONE_TWO_MTHREE=circshift(circshift(circshift(A,1,1),-1,2),1,3) + A;
MONE_MTWO_THREE=circshift(circshift(circshift(A,1,1),1,2),-1,3) + A;
MONE_MTWO_MTHREE=circshift(circshift(circshift(A,1,1),1,2),1,3) + A;


% Being equal to 2 will mean that both sides are above abs thresh. So we'll find 
% those places. Also lets trim the thing down.


ONE = ONE(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) > 1.5;
TWO= TWO(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) > 1.5;
THREE= THREE(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) > 1.5;
MONE= MONE(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) > 1.5;
MTWO= MTWO(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) > 1.5;
MTHREE= MTHREE(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) > 1.5;

ONE_TWO= ONE_TWO(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) > 1.5;
ONE_MTWO= ONE_MTWO(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) > 1.5;
ONE_THREE= ONE_THREE(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) > 1.5;
ONE_MTHREE= ONE_MTHREE(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) > 1.5;

MONE_TWO= MONE_TWO(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) > 1.5;
MONE_MTWO= MONE_MTWO(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) > 1.5;
MONE_THREE= MONE_THREE(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) > 1.5;
MONE_MTHREE= MONE_MTHREE(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) > 1.5;

TWO_THREE = TWO_THREE(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) > 1.5;
TWO_MTHREE= TWO_MTHREE(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) > 1.5;

MTWO_THREE= MTWO_THREE(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) > 1.5;
MTWO_MTHREE= MTWO_MTHREE(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) > 1.5;

ONE_TWO_THREE =ONE_TWO_THREE(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) > 1.5;
ONE_TWO_MTHREE=ONE_TWO_MTHREE(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) > 1.5;
ONE_MTWO_THREE=ONE_MTWO_THREE(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) > 1.5;
ONE_MTWO_MTHREE=ONE_MTWO_MTHREE(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) > 1.5;

MONE_TWO_THREE =MONE_TWO_THREE(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) > 1.5;
MONE_TWO_MTHREE=MONE_TWO_MTHREE(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) > 1.5;
MONE_MTWO_THREE=MONE_MTWO_THREE(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) > 1.5;
MONE_MTWO_MTHREE=MONE_MTWO_MTHREE(2:L(1)+1, 2:L(2)+1, 2:L(3)+1) > 1.5;




%Now lets get the linear indecies. This is important because that's how we'll index our graph.

TONE =find(ONE > 0)'; 
TTWO=find(TWO > 0)';
TTHREE=find(THREE > 0)';
TMONE=find(MONE > 0)';
TMTWO=find(MTWO > 0)';
TMTHREE=find(MTHREE > 0)';

TONE_TWO =find(ONE_TWO > 0)';
TONE_MTWO =find(ONE_MTWO > 0)';
TONE_THREE =find(ONE_THREE > 0)';
TONE_MTHREE =find(ONE_MTHREE > 0)';

TMONE_TWO =find(MONE_TWO > 0)';
TMONE_MTWO=find(MONE_MTWO > 0)';
TMONE_THREE=find(MONE_THREE > 0)';
TMONE_MTHREE=find(MONE_MTHREE > 0)';

TTWO_THREE =find(TWO_THREE > 0)';
TTWO_MTHREE=find(TWO_MTHREE > 0)';

TMTWO_THREE=find(MTWO_THREE > 0)';
TMTWO_MTHREE=find(MTWO_MTHREE > 0)';

TONE_TWO_THREE =find(ONE_TWO_THREE > 0)';
TONE_TWO_MTHREE=find(ONE_TWO_MTHREE > 0)';
TONE_MTWO_THREE=find(ONE_MTWO_THREE > 0)';
TONE_MTWO_MTHREE=find(ONE_MTWO_MTHREE > 0)';

TMONE_TWO_THREE=find(MONE_TWO_THREE > 0)';
TMONE_TWO_MTHREE=find(MONE_TWO_MTHREE > 0)';
TMONE_MTWO_THREE=find(MONE_MTWO_THREE > 0)';
TMONE_MTWO_MTHREE=find(MONE_MTWO_MTHREE > 0)';



%Putting it all together. Since the length of each individual vector created above is dynamic,
% to define the weight vector we sorta have to do it this way

Total_single=[TONE TTWO TTHREE TMONE TMTWO TMTHREE];
Total_double=[TONE_TWO TONE_MTWO TONE_THREE TONE_MTHREE TMONE_TWO TMONE_MTWO TMONE_THREE TMONE_MTHREE TTWO_THREE TTWO_MTHREE TMTWO_THREE TMTWO_MTHREE];
Total_triple=[TONE_TWO_THREE TONE_TWO_MTHREE TONE_MTWO_THREE TONE_MTWO_MTHREE TMONE_TWO_THREE TMONE_TWO_MTHREE TMONE_MTWO_THREE TMONE_MTWO_MTHREE];
Total=[Total_single Total_double Total_triple];

%The weight vector helps the graph distance actual resemble Euclidean. As opposed to \ell_1 distance
% which it would be if it wasn't included. Note that shifting twice is moving in two dimensions and 
% shifting three times is moving in three dimensions, hence the quadrature terms.

WeightVec=[ones(1,length(Total_single)) ones(1,length(Total_double))*sqrt(2) ones(1,length(Total_triple))*sqrt(3)];



display('Populating adjacency matrix...')
display('Single Shifts (6)....')
display(datestr(now))
display('1')

% The offset is just to make it so we don't have to constantly run sub2ind, which is 
% actually startilingly slow. Who knew?

[x y z]=ind2sub(size(IMG), TONE(1)) ;
OFFSET = (-1*TONE(1)) +sub2ind(size(IMG), (x+1), y, z);
nums=TONE + OFFSET;
V=min(IMG(TONE), IMG(TONE+OFFSET));
display(datestr(now))


display('2')

%If you don't understand what nums and V do, just wait till we define the sparse 
%matrix at the end. Then it'll be clearer. Originally the idea was to fill in the
%sparse matrix as we went. That took HOURS, even if it was appropriately preallocated
% I'm still not sure why, honestly. Probably because preallocating isn't super 
%effective in the sparse universe?

OFFSET = (-1*TONE(1)) + sub2ind(size(IMG), (x), y+1, z);
nums=[nums TTWO + OFFSET];
V=[V min(IMG(TTWO), IMG(TTWO+OFFSET))];

display(datestr(now))
display('3')
OFFSET = (-1*TONE(1)) + sub2ind(size(IMG), (x), y, z+1);
nums=[nums TTHREE + OFFSET];
V=[V min(IMG(TTHREE), IMG(TTHREE+OFFSET))];


display('4')
OFFSET = (-1*TONE(1)) + sub2ind(size(IMG), (x-1), y, z);
nums=[nums TMONE + OFFSET];
V=[ V min(IMG(TMONE), IMG(TMONE+OFFSET))];

display('5')
OFFSET = (-1*TONE(1)) + sub2ind(size(IMG), (x), y-1, z);
nums=[nums TMTWO+OFFSET];
V=[V min(IMG(TMTWO), IMG(TMTWO+OFFSET))];


display('6')

OFFSET = (-1*TONE(1)) + sub2ind(size(IMG), (x), y, z-1);
nums=[nums TMTHREE + OFFSET];
V=[V min(IMG(TMTHREE), IMG(TMTHREE+OFFSET))];

display('Double Shifts...(12)')


display(datestr(now))

display('1')
OFFSET = (-1*TONE(1)) + sub2ind(size(IMG), (x+1), y+1, z);
nums=[nums TONE_TWO + OFFSET];
V=[V min(IMG(TONE_TWO), IMG(TONE_TWO+OFFSET))];


display('2')
OFFSET = (-1*TONE(1)) + sub2ind(size(IMG), (x+1), y-1, z);
nums=[nums TONE_MTWO + OFFSET];
V=[V min(IMG(TONE_MTWO), IMG(TONE_MTWO+OFFSET))];



display('3')
OFFSET = (-1*TONE(1)) + sub2ind(size(IMG), (x+1), y, z+1);
nums=[nums TONE_THREE + OFFSET];
V=[V min(IMG(TONE_THREE), IMG(TONE_THREE+OFFSET))];


display('4')
OFFSET = (-1*TONE(1)) + sub2ind(size(IMG), (x+1), y, z-1);
nums=[nums TONE_MTHREE + OFFSET];
V=[V min(IMG(TONE_MTHREE), IMG(TONE_MTHREE+OFFSET))];



display('5')
OFFSET = (-1*TONE(1)) + sub2ind(size(IMG), (x-1), y+1, z);
nums=[nums TMONE_TWO + OFFSET];
V=[V min(IMG(TMONE_TWO), IMG(TMONE_TWO+OFFSET))];


display('6')
OFFSET = (-1*TONE(1)) + sub2ind(size(IMG), (x-1), y-1, z);
nums=[nums TMONE_MTWO + OFFSET];
V=[V min(IMG(TMONE_MTWO), IMG(TMONE_MTWO+OFFSET))];


display('7')
OFFSET = (-1*TONE(1)) + sub2ind(size(IMG), (x-1), y, z+1);
nums=[nums TMONE_THREE + OFFSET];
V=[V min(IMG(TMONE_THREE), IMG(TMONE_THREE+OFFSET))];

display('8')
OFFSET = (-1*TONE(1)) + sub2ind(size(IMG), (x-1), y, z-1);
nums=[nums TMONE_MTHREE + OFFSET];
V=[V min(IMG(TMONE_MTHREE), IMG(TMONE_MTHREE+OFFSET))];

display(datestr(now))

display('9')
OFFSET = (-1*TONE(1)) + sub2ind(size(IMG), (x), y+1, z+1);
nums=[nums TTWO_THREE + OFFSET];
V=[V min(IMG(TTWO_THREE), IMG(TTWO_THREE + OFFSET))];

display('10')
OFFSET = (-1*TONE(1)) + sub2ind(size(IMG), (x), y+1, z-1);
nums=[nums TTWO_MTHREE + OFFSET];
V=[V min(IMG(TTWO_MTHREE), IMG(TTWO_MTHREE+OFFSET))];



display('11')
OFFSET = (-1*TONE(1)) + sub2ind(size(IMG), (x), y-1, z+1);
nums=[nums TMTWO_THREE + OFFSET];
V=[V min(IMG(TMTWO_THREE), IMG(TMTWO_THREE+OFFSET))];


display('12')
OFFSET = (-1*TONE(1)) + sub2ind(size(IMG), (x), y-1, z-1);
nums=[nums TMTWO_MTHREE + OFFSET];
V=[V min(IMG(TMTWO_MTHREE), IMG(TMTWO_MTHREE+OFFSET))];

display('Triple Shifts...(8)')

display('1')
OFFSET = (-1*TONE(1)) + sub2ind(size(IMG), (x+1), y+1, z+1);
nums=[nums TONE_TWO_THREE + OFFSET];
V=[V min(IMG(TONE_TWO_THREE), IMG(TONE_TWO_THREE + OFFSET))];


display('2')
OFFSET = (-1*TONE(1)) + sub2ind(size(IMG), (x+1), y+1, z-1);
nums=[ nums TONE_TWO_MTHREE + OFFSET];
V=[V min(IMG(TONE_TWO_MTHREE), IMG(TONE_TWO_MTHREE + OFFSET))];



display('3')
OFFSET = (-1*TONE(1)) + sub2ind(size(IMG), (x+1), y-1, z+1);
nums=[nums TONE_MTWO_THREE + OFFSET];
V=[V min(IMG(TONE_MTWO_THREE), IMG(TONE_MTWO_THREE + OFFSET))];


display('4')
OFFSET = (-1*TONE(1)) + sub2ind(size(IMG), (x+1), y-1, z-1);
nums=[nums TONE_MTWO_MTHREE + OFFSET];
V=[V min(IMG(TONE_MTWO_MTHREE), IMG(TONE_MTWO_MTHREE + OFFSET))];


display('5')
OFFSET = (-1*TONE(1)) + sub2ind(size(IMG), (x-1), y+1, z+1);
nums=[nums TMONE_TWO_THREE + OFFSET];
V=[V min(IMG(TMONE_TWO_THREE), IMG(TMONE_TWO_THREE + OFFSET))];

display('6')
OFFSET = (-1*TONE(1)) + sub2ind(size(IMG), (x-1), y+1, z-1);
nums=[nums TMONE_TWO_MTHREE + OFFSET];
V=[V min(IMG(TMONE_TWO_MTHREE), IMG(TMONE_TWO_MTHREE + OFFSET))];

display('7')
OFFSET = (-1*TONE(1)) + sub2ind(size(IMG), (x-1), y-1, z+1);
nums=[nums TMONE_MTWO_THREE + OFFSET];
V=[V min(IMG(TMONE_MTWO_THREE), IMG(TMONE_MTWO_THREE + OFFSET))];

display('8')
OFFSET = (-1*TONE(1)) + sub2ind(size(IMG), (x-1), y-1, z-1);
nums=[nums TMONE_MTWO_MTHREE + OFFSET];
V=[V min(IMG(TMONE_MTWO_MTHREE), IMG(TMONE_MTWO_MTHREE + OFFSET))];


disp('making the matricies...')
Conn=sparse(Total, nums, V,T, T);
Weight=sparse(Total,nums,WeightVec, T, T);

%the weight matrix should be COMPONENTWISE multipled by a binarized version of sparse and then fed into Matlab's GRAPH and Distances functions. Matlab can handle these huge graphs with impressive speed (it's all just Dijskstra's
% algorithm anyway, and since the graph has low degree it's very efficient) 




