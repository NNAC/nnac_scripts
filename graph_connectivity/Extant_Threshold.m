X=load_untouch_nii('spmT_0001.nii');
IMG=X.img;
%change height threshold
thresh=3.85;
[Conn, Weight] = Create_Graph_Connectivity(thresh,IMG);

Conn_thresh_weight = ((Conn  > thresh).*Weight);
G=graph(Conn_thresh_weight);
bins=conncomp(G);
binsu=unique(bins);
[N,EDGES] = histcounts(bins,binsu);
%change voxel extent threshold below
L = (N > 150);
T=find(L);
NEWIMG=0*IMG;
S=zeros(1,length(bins));
for i=1:length(T)
S( bins==EDGES(T(i))) =1;
end
for i=1:length(S)
NEWIMG(i)=S(i)*IMG(i);
end
X.img=NEWIMG;
save_untouch_nii(X, 'tau_threshold.nii');